@ECHO OFF
ECHO ---------------------------------
ECHO ....... Servers Set Up ..........
ECHO ---------------------------------

ECHO Start Zookeeper
start set_up_zookeeper.bat

timeout /t 25

ECHO Start Kafka
start set_up_kafka.bat

