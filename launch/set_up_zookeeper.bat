@ECHO OFF
ECHO ---------------------------------
ECHO ....... Zookeeper Set Up ........
ECHO ---------------------------------

ECHO Go to Zookeeper directory
cd C:\Zookeeper\bin

ECHO Start Zookeeper server
CALL zkServer.cmd
