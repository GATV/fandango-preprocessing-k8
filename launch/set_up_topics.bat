@ECHO OFF
ECHO ---------------------------------
ECHO ....... Kafka Set Up .........
ECHO ---------------------------------

cd C:\Kafka\bin\windows
ECHO Create New Topic: input_raw
CALL kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic input_raw_2
ECHO Done

ECHO Create New Topic: input_preprocessed
CALL kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic input_preprocessed_2
ECHO Done

ECHO Create New Topic: analyzed_auth_org
CALL kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic analyzed_auth_org_2
ECHO Done

