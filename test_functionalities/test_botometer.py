import botometer

# Twitter Data
CONSUMER_KEY = "WcIJMyBF3spFEtRGHwqDlAKDT"
CONSUMER_SECRET = "IfNlsgNrW5o6rfhayOURF1BcUDBcsrfgqss9g5inqeFytDbkgE"
ACCESS_TOKEN = "973174862159253505-mAYpqjzegRXFNhdEPXOkDLsHWXePp7q"
ACCESS_TOKEN_SECRET = "0mmfQcNDJBIFhMM9bexZSO1eIKhFdaP8JX9cMnBG81gJE"
# =========================================================

rapidapi_key = "2f1ecb1dbamsh53a3bc12edf61c3p1ebec2jsn293407fa2863"
twitter_app_auth = {
    'consumer_key': CONSUMER_KEY,
    'consumer_secret': CONSUMER_SECRET,
    'access_token': ACCESS_TOKEN,
    'access_token_secret': ACCESS_TOKEN_SECRET,
  }
bom = botometer.Botometer(wait_on_ratelimit=True,
                          rapidapi_key=rapidapi_key,
                          **twitter_app_auth)

twitter_account_id: int = 2174559821

result = bom.check_account(twitter_account_id)

"""
The response object has five main pieces:

User is exactly the user object from the request. We return this to you untouched with asynchronous workflows in mind,
 so that you don't have to worry about keeping the user data and matching it up to the appropriate response later.
Scores contains the overall classification results. The english score uses all six categories of features, while the
 universal score omits sentiment and content features, both of which are English-specific.
Categories gives subscores for each of the six feature classes.
CAP Complete Automation Probability (CAP) is the probability, according to our models, that this account is completely
 automated, i.e., a bot. This probability calculation uses Bayes' theorem to take into account an estimate of the
  overall prevalence of bots, so as to balance false positives with false negatives.
Display_scores are bascially the corresponding original scores multiplied by 5. Display scores are shown
 on https://botometer.iuni.iu.edu/ .

"""