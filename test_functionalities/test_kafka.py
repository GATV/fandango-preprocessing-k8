from kafka import KafkaProducer
from json import dumps
from input_examples.input_raw_examples import data_ex_1

data: dict = data_ex_1

producer: KafkaProducer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                        value_serializer=lambda x:
                                        dumps(x).encode('utf-8'))
topic_producer: str = "input_raw_3"
producer.send(topic=topic_producer, value=data)

